#ReadMe#
---------

A project on creating a robot with emotions was worked on by Dr. Platinum for a long time. It went well, however expressing feelings of mostly hatred. It created newer models of itself. The last two were based on living people. Meanwhile, a boy goes on a journey to find out about his past, and what he finds out…

Fragmented Memories of a Lost Past is a text-based adventure RPG similar to those from the old days. A graphical version, similar to old JRPGs, such as Dragon Warrior or Mother, is planned, but not yet in development.

---------

FMoaLP requires no dependences other than python3. It should work out of the box on most systems.
**WINDOWS USERS**: Install Python 3.x from [the website](https://www.python.org/)