class item:
	def __init__(self, name, value, desc, equ):
		self.name = name
		self.value = value
		self.desc = desc
		self.equ = equ

class potion(item):
	def __init__(self):
		self.name = "Potion"
		self.value = 20
		self.desc = "Heals HP"
		self.equ = 0

class weapon(item):
	def __init__(self, name, value, desc, power):
		self.name = name
		self.value = value
		self.desc = desc
		self.equ = 1
		self.power = power

# Swords
class sword(weapon):
	def __init__(self, name, value, desc, power):
		self.name = name
		self.value = value
		self.desc = desc
		self.equ = 1
		self.power = power

swordtest = sword("102114 Sword", 0, "A sword radiating mysterious power. Where did this come from?", 800)
sword1 = sword("Wooden Sword", 25, "The weakest weapon", 2)
lightsword = sword("Sword of Light", 0, "A mythical sword that could rival the power of even Project child()", 900)

# Bows and Guns. Generally more powerful than swords but miss more often and requre ammuition.

class bow(weapon):
	def __init__(self, name, value, desc, power, accuracy, ammo, ammomax):
		self.name = name
		self.value = value
		self.desc = desc
		self.equ = 1
		self.power = power
		self.accuracy = accuracy
		self.ammo = ammo
		self.ammomax = ammomax

bowtest = bow("102114 Bow", 0, "A bow radiating mysterious power. Where did this come from?", 800, 90, 500, 500)

#Misc. Items

testitem = item("102114 Key", 0, "What is this?", 0)