import json
import os
from . import item
from random import randint

class character(object):
	def __init__(self, name, hp, hpmax, atk, mag, defense, spd, lvl, exp, exptil, money, alive, inv, mp, mpmax):
		self.name = name
		self.hp = hp
		self.hpmax = hpmax
		self.atk = atk
		self.mag = mag
		self.defense = defense
		self.spd = spd
		self.lvl = lvl
		self.exp = exp
		self.exptil = exptil
		self.money = money
		self.alive = alive
		self.inv = inv
		self.mp = mp
		self.mpmax = mpmax
	def to_JSON(self):
		return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)
	def attack(self, enemy):
		clear()
		print("%s attacks!" % self.name)
		if randint(1,45) == 1:
			print("CRITIAL HIT!")
			enemy.hp -= (self.atk + (int(self.atk * 1.5)) - enemy.defense)
			display = (self.atk + (int(self.atk * 1.5)) - enemy.defense)
			pause()
			clear()
			print("%s did %s damage to the enemy!" % (self.name, display))
			pause()
		elif randint(1,25) == 9:
			print("%s missed..." % self.name)
			pause()
		else:
			enemy.hp -= (self.atk - enemy.defense)
			display = (self.atk - enemy.defense)
			pause()
			clear()
			print("%s did %s damage to the enemy!" % (self.name, display))
			pause()
	def usePotion(self):
		clear()
		if self.inv['pots'] < 1:
			print("%s does not have any potions..." % self.name)
			pause()
		else:
			self.hp = self.hpmax
			print("%s used a potion!" % self.name)
			self.inv['pots'] -= 1
			pause()
	def levelup(self):
		clear()
		print("You leveled up!")
		atkup = randint(1,3)
		magup = randint(1,3)
		defup = randint(1,3)
		spdup = randint(1,3)
		hpup = randint(3,7)
		mpup = randint(1,2)
		self.lvl += 1
		self.exp = 0
		self.exptil += (int(self.exptil * 0.25))
		pause()
		print("%s is now level %s" % (self.name, self.lvl))
		pause()
		print("Atk +%s" % atkup)
		self.atk += atkup
		pause()
		print("Magic +%s" % magup)
		self.mag += mpup
		pause()
		print("Def +%s" % defup)
		self.defense += defup
		pause()
		print("Spd +%s" % spdup)
		self.spd += spdup
		pause()
		print("HP +%s" % hpup)
		self.hpmax += hpup
		pause()
		print("MP +%s" % mpup)
		self.mpmax += mpup
		pause()
		self.hp = self.hpmax
		self.mp = self.mpmax

invdefault = {"pots": 10, "magicpots": 10, "weapon": item.swordtest, "other": [item.testitem, item.bowtest]}

### This system at the moment only supports 4 characters.

# These four lines define characters. Feel free to edit them for your project.
beta = character("Beta", 15, 15, 5, 3, 1, 3, 1, 0, 15, 0, 1, invdefault, 5, 5)
alpha = character("Alpha", 78, 79, 27, 28, 29, 18, 15, 0, 515, 0, 0, invdefault, 23, 23)
sonata = character("Sonata", 104, 104, 39, 39, 30, 34, 20, 0, 1024, 0, 0, invdefault, 30, 30)
ted = character("Ted", 234, 234, 54, 39, 29, 40, 49, 0, 10457, 0, 0, invdefault, 50, 50)

# Edit this line to add characters to the charlist for battles.
charlist = [beta, alpha, sonata, ted]

# This line determines how many characters there are for battling sequence. Set this to how many characters the player can control at the BEGINNING at the game. Edit this variable in your main script(s)
charnum = 4

def clear(): # Just a function to replace the long line that improves portability when clearing the screen.
	os.system('cls' if os.name == 'nt' else 'clear')
def cinput(message): # Grabs input
	global temp
	temp = input(message)
def pause(): # Wait for the user to press enter
	temp = input()