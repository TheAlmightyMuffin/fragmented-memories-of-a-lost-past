import os

class enemy(object):
	def __init__(self, name, hp, atk, defense, spd, drop, exp):
		self.name = name
		self.hp = hp
		self.atk = atk
		self.defense = defense
		self.spd = spd
		self.drop = drop
		self.exp = exp
	def attack(self, enemy):
		clear()
		print("%s attacks!" % self.name)
		enemy.hp -= (self.atk - enemy.defense)
		pause()
		clear()
		print("%s did %s damage to %s!" % (self.name, (self.atk - enemy.defense), enemy.name))
		pause()
	def heal(self):
		pass

def clear(): # Just a function to replace the long line that improves portability when clearing the screen.
	os.system('cls' if os.name == 'nt' else 'clear')
def cinput(message): # Grabs input
	global temp
	temp = input(message)
def pause(): # Wait for the user to press enter
	temp = input()