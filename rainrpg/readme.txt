Rainfall RPG Engine
A WIP text-based RPG engine for python.

**This version was slightly modified for FMoaLP.**


To use: 'import rainrpg' or 'from rainrpg import *'

To add a character, please edit character.py
To add an item, please edit item.py
To add magic, please edit magic.py
Enemies and shops can be added in your main script.

For now, you'll have to edit battle.py to change characters and the number of characters.