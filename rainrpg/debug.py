from . import item

def debugMenu():
    print("Rainfall RPG Engine Debug")
    print("I won't stop you")
    print("Just type 'help'")
    while True:
        temp = input("> ")
        if temp == "help":
            print("Hiya! This is a debug screen.")
            print("'character' to see character values. Might be best to put all the characters in a list.")
            print("'item' to see item values. Might be best to put all the items in a list.")
            print("'exit' to quit")
            print("That's all you need to know.")
        if temp == "character":
            print("error")
        if temp == "items":
            print(item.items)
        if temp == "exit":
            quit()