#!/usr/bin/env python2

# I'm trying to clean up this mess I made! I swear!

import os
import time
import json

from rainrpg import *

global misc
misc = {'charnum' : charnum, 'room' : 1} 

def title():
	clear()
	print("Fragmented Memories of a Lost Past")
	print("Pre-Alpha 0.0.4 (Build 22)")
	print("\n1) New Game")
	print("2) Load Game")
	print("3) Quit")
	cinput(">> ")
	if temp == "1":
		opening()
	if temp == "2":
		load()
	if temp == "3":
		quit()
	if temp == "07friend":
		debug()

def opening():
	clear()
	time.sleep(2)
	print("Who am I?")
	print("\n        Where is this?")
	print("\n                       Why can't I remember anything?")
	time.sleep(5)
	clear()
	time.sleep(3)
	print("FRAGMENTED MEMORIES")
	time.sleep(2)
	print("OF A LOST PAST")
	time.sleep(5)
	clear()
	time.sleep(4)
	print("Huh? Who are you?")
	time.sleep(4)
	clear()
	print("???: B.t...A.p.a...o.a.a...T.d...")
	time.sleep(5)
	clear()
	print("???: Bet...A.pha..Son..a...Te...")
	time.sleep(4)
	clear()
	print("???: Beta...")
	time.sleep(8)
	beginning()
	
def beginning():
	misc['room'] = 1
	clear()
	time.sleep(2)
	print("???: Oh good! I think he's waking up!")
	pause()
	print("???: ...Codename \"Beta\"? What does that mean?")
	pause()
	print("***You open your eyes to see a man in a white lab coat. Nothing much interesting about him.")
	pause()
	print("???: Oh, you must be confused, waking up somewhere you've never been, and having")
	print("someone talk to you as you wake up.")
	pause()
	print("???: My name is... of no importance... Just call me \"Doctor\"")
	pause()
	print("Doctor: Beta... stay here. No need to get yourself hurt again.")
	pause()
	print("Doctor: Can you speak?")
	pause()
	print("***You open your mouth to speak. It hurts at first, but then you start to speak.")
	pause()
	print("Beta: Yeah, I think.")
	pause()
	print("Doctor: Thank goodness! Go ahead and stay here.")
	pause()
	clear()
	time.sleep(2)
	print("Beta: Doctor, who exactly am I? I...")
	pause()
	print("Doctor: Don't you remember?")
	pause()
	print("Beta: ...N-no.")
	pause()
	print("Doctor: Oh... Well...")
	pause()
	print("***You wait for an answer, but he never says anything.")
	pause()
	hub_doctor()


def hub_doctor():
	while True:
		clear()
		print("%s/%s HP" % (beta.hp, beta.hpmax))
		print("Level %s" % beta.lvl)
		print("\nThe Doctor's Abode")
		print("\n1) Proceed")
		print("2) Save")
		print("3) Quit")
		cinput(">> ")
		if temp == "1":
			sceneDoctor2()
		elif temp == "2":
			save()
		elif temp == "3":
			exit()
		elif temp == "07friend":
			debug()





def sceneMeetAlpha():
	clear()
	misc['room'] = 0
	print("***You almost fall off of a cliff. However, a person grabs your hand before you")
	print("fall to your doom.")
	pause()
	print("Beta: ...Huh?")
	pause()
	print("???: Oh my, you almost fell!")
	pause()
	print("Beta: Who are you?")
	pause()
	clear()
	print("Alpha: Um... I'm Alpha. I-I'm not... really sure if that's my real name, though.")
	pause()
	print("Beta: Well, my name is Beta")
	pause()
	print("Alpha: Nice to meet you, Beta")
	pause()
	print("Beta: Do you have anywhere to go?")
	pause()
	print("Alpha: Oh... uh... no, actually. Uh, mind if I tag along with you?")
	pause()
	print("Beta: Well, why not?")
	alpha.have = 1
	pause()
	
def sceneMeetSonata():
	clear()
	misc['room'] = 0
	print("***Deep in the Levvi Forest")
	pause()
	print("???: All this... could have been avoided... if... I wasn't in the way...")
	pause()
	print("NO! ARRGH! No... no...")
	pause()
	clear()
	print("Anon: We have track of the last Levvi. Bring her to the Anonymous Headquarters.")
	print("She's mad about something, though...")
	pause()
	print("Huh? Beta?")
	print("***Anon calls Patch")
	pause()
	print("Anon: Patch, we've got a real emergency. Beta is in the Levvi Forest.")
	print("Patch: Well, what are you standing around for? Keep him away from the Levvi.")
	print("Anon: It might be too late for that.")
	print("Patch: BUT YOU ARE ANONYMOUS! YOU ARE, LIKE, THE GREATEST BEING ON CONCORDIA!")
	print("Anon: This is not time for games, Patch.")
	print("Patch: Okay... okay... New plan... We kill Beta and get the Levvi.")
	print("Anon: I didn't want to resort to murder, but whatever we have to do...")
	pause()
	clear()
	print("???: Huh, who's that?")
	print("Beta: Don't worry. I'm not here to take you away. My name is Beta")
	print("???: Oh, uh, hello. My name is Sonata. Nice to meet you.")
	print("Beta: What happened here?")
	pause()
	print("Sonata: The Bass Clan attacked, and it's all my fault... All my fault...")
	print("Beta: Huh?")
	print("Sonata: If I hadn't gotten in the way, this village would still be here.")
	print("Beta: Don't worry, Sonata. It's not your fault. Team Melody tried their best to defend this place.")
	print("Sonata: And they would succeeded if it weren't for me.")
	print("Beta: Well, where are your parents?")
	print("Sonata: My parents? They're... gone...")
	print("Beta: Gone? To where?")
	print("Sonata: ...")
	print("Beta: ...")
	print("Sonata: ...Maybe we can go find my sister, Cantata. Together.")
	print("Beta: Sure.")
	print("Sonata: Beta, you're a really nice human. I thought they were all big meanies")
	sonata.have = 1
	pause()
	

def testscene(): # This is the test scene. I might use it for a tutorial level or something
	global lb
	global foreshadowletter
	foreshadowletter = 0
	lb = "nope"
	clear()
	print("TUTORIAL LEVEL")
	print("----------------------------------------\n")
	print("You have no idea how you got here.")
	print("You see a bush in the distance.")
	print("You see a tree in the distance.")
	while True:
		ginput()
		if temp == "walk to bush":
			print("You are at the bush. Not much here.")
			lb = "bush"
		if temp == "take letter":
			if lb == "tree":
				if foreshadowletter == 1:
					print("You already took the letter.")
				print("You took the letter.")
				foreshadowletter = 1
		if temp == "walk to tree":
			print("You see a letter on the ground")
			lb = "tree"
		if temp == "look":
			if lb == "tree":
				print("You are next to a tree.")
				print("You still don't know how you got here.")
			if lb == "bush":
				print("You are next to a bush.")
				print("You still don't know how you got here.")
			print("You have no idea how you got here.")
			print("You see a bush in the distance.")
			print("You see a tree in the distance.")
	
	
def ginput(): # Grabs input for gameplay outside battle. Comes with predefined features.
	cinput(">> ")
	if temp == "attack":
		print("Attack what?")
	if temp == "take":
		print("Take what?")
	if temp == "give":
		print("Give what to who?")
	if temp == "inventory":
		inventory()
	if temp == "talk":
		print("Talk to who?")
	if temp == "help":
		gamehelp()
	if temp == "save":
		save()
	if temp == "quit":
		title()
	pass
	
def inventory():
	clear()
	print("Items:")
	print("\n%s" % beta.inv)
	if temp == "999":
		letter0()
	pass
	
def gamehelp():
	print("walk (DIRECTION) or to (TARGET) - Walk in a direction (north, east, south, west) or towards a target.")
	print("look [OBJECT] - Look around. If specified, will look at an object")
	print("attack (TARGET) - Attack an enemy or object")
	print("take (ITEM) - Take an item")
	print("give (ITEM) (TARGET) - Give an item")
	print("inventory - Open your inventory")
	print("talk (TARGET) - Talk to someone or something")
	print()
	print("Other commands may be available in certain situations.")
	
def letter0():
	clear()
	print("I think we found her. Meet me at the lab")
	print("in 1700 hours.")
	print()
	print("                          Codename: Easy")
	pause()
	inventory()

def clear(): # Just a function to replace the long line that improves portability when clearing the screen.
	os.system('cls' if os.name == 'nt' else 'clear')
def cinput(message): # Grabs input
	global temp
	temp = input(message)
def pause(): # Wait for the user to press enter
	temp = input()
def stats():
	print(beta.name)
	print("\nYour HP:", beta.hp)
	print("MP:", beta.mp)
	print("\nLevel", beta.lvl)
	print("EXP:", beta.exp)
	print("\nAttack:", beta.atk)
	print("Base Magic:", beta.mag)
	print("Defense:", beta.defense)
	print("Speed:", beta.spd)
	print("\nItems: ", beta.inv)
	print("\n%s until next level." % beta.exptil)

def debug(): # Debug function for easy... debug. Be careful when using this!
	cinput(">> ")
	if temp == "start":
		opening()
	if temp == "gamehelp":
		gamehelp()
	if temp == "testscene":
		testscene()
	if temp == "battletest":
		battletest()
	if temp == "opening":
		opening()
	if temp == "beginning":
		beginning()
	if temp == "sceneMeetAlpha":
		sceneMeetAlpha()
	if temp == "sceneMeetSonata":
		sceneMeetSonata()
	if temp == "stats":
		stats()
	if temp == "levelup":
		debug_lvlup()
	if temp == "betalvl":
		betalvlup()
	if temp == "betalvl1":
		beta.levelup()
	if temp == "alphalvl":
		alphalvlup()
	if temp == "sonatalvl":
		sonatalvlup()
	if temp == "tedlvl":
		tedlvlup()
	if temp == "raindebug":
		debugMenu()
	print("Things did not go according to plan")
	debug()

# For debug :p
def betalvlup():
	beta.levelup()
	beta.levelup()
	beta.levelup()
	beta.levelup()
	beta.levelup()
	beta.levelup()
	beta.levelup()
	beta.levelup()
	beta.levelup()
	beta.levelup()
	beta.levelup()
	beta.levelup()
	beta.levelup()
	beta.levelup()

def alphalvlup():
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()
	alpha.levelup()

def sonatalvlup():
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()
	sonata.levelup()

def tedlvlup():
	ted.levelup()
	ted.levelup()
	ted.levelup()
	ted.levelup()
	ted.levelup()
	ted.levelup()
	ted.levelup()
	ted.levelup()
	ted.levelup()
	ted.levelup()
	ted.levelup()
	ted.levelup()
	ted.levelup()
	ted.levelup()
	ted.levelup()
	ted.levelup()
	ted.levelup()
	ted.levelup()
	ted.levelup()
	ted.levelup()
	ted.levelup()

def debug_lvlup():
	beta.levelup()
	alpha.levelup()
	sonata.levelup()
	ted.levelup()
	beta.levelup()
	alpha.levelup()
	sonata.levelup()
	ted.levelup()
	beta.levelup()
	alpha.levelup()
	sonata.levelup()
	ted.levelup()
	beta.levelup()
	alpha.levelup()
	sonata.levelup()
	ted.levelup()

def battletest():
	entest = enemy("HamBot", 15, 6, 2, 7, 50, 50)
	battle(entest)
	print("This was a test battle. Not ment to be in the final product")
	print("If you want more, check out the storyline.")
	pause()
	title()

def battletest2():
	entest = enemy("|", 9000, 9000, 9000, 9000, 50, 50)
	battle(entest)
	print("die")
	title()

def battletest3():
	entest = enemy("Ted", 234, 54, 29, 40, 50, 50)
	battle(entest)
	print("I'll join you")
	title()
	
def save():
	with open('data/save1.json', 'wb') as fp:
	    json.dump(beta.to_JSON(), fp, indent=2)
	with open('data/save2.json', 'wb') as fp:
	    json.dump(alpha.to_JSON(), fp, indent=2)
	with open('data/save3.json', 'wb') as fp:
	    json.dump(sonata.to_JSON(), fp, indent=2)
	with open('data/save4.json', 'wb') as fp:
	    json.dump(ted.to_JSON(), fp, indent=2)
	with open('data/save5.json', 'wb') as fp:
	    json.dump(misc, fp, indent=2)
	with open('data/save6.json', 'wb') as fp:
		json.dump("uckfay ouyay ambothay", fp, indent=2)
	print("Save Complete!")
	pause()
def load():
	with open('data/save1.json', 'rb') as fp:
		beta = json.load(fp)
	with open('data/save2.json', 'rb') as fp:
	    alpha = json.load(fp)
	with open('data/save3.json', 'rb') as fp:
	    sonata = json.load(fp)
	with open('data/save4.json', 'rb') as fp:
	    ted = json.load(fp)
	with open('data/save5.json', 'rb') as fp:
	    misc = json.load(fp)
	if misc['room'] == 1:
		beginning()
	elif misc['room'] == 2:
		sceneOne()
	elif misc['room'] == 3:
		scenePatchOne()
	elif misc['room'] == 4:
		sceneParadeIsntToday()
	else:
		print("Something didn't go according to plan. [Error 1]")
		pause()
		title()

def exit():
	print("Any unsaved progress will be lost.")
	print("Are you sure you want to quit? y/n")
	cinput(">> ")
	if temp == "y":
		quit()
	else:
		pass
	
title()
